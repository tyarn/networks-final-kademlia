#include "helper.h"
#include <string>
#include <string.h>
#include "Constants.h"
#include <iostream>
#include "kBucket.h"
#include "LinkedList.h"
#include "triple.h"
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include "socketException.h"
using namespace std;

//PRE:arrray is a character array
//POST:takes the number specified in chars in the array and makes it into an int
int charToInt(char array[]){
  int counter = 0;
  bool negative = false;
  while(array[counter + 1] != EOS){
    counter++;
  }
  //only have this for forloop here so i can change it if number is
  //negative
  int counter2 = 0;
  //ASSERT:counter is at position before EOS
  if(array[0] == NEGATIVE){
    //ASSERT:number is negative
    counter --;//deiterate counter to reflect we have one less num to
	       //multiply
    counter2 = 1;//change array index so we dont get value of -
    negative = true;
  }
  int multiplier = 1;
  for(int index = 0; index < counter; index ++){
    multiplier *= BASE;
  }
  //ASSERT:multiplier is at correct value
  int returnNum = 0;
  while(array[counter2] != EOS){
    int number =(int)array[counter2] - ASCIIZERO;//ASSERT:number holds char in int form
    number = number * multiplier;
    multiplier /= BASE;
    returnNum = returnNum + number;
    counter2++;
  }
  if(negative){
    //ASSERT:number is negative
    returnNum = returnNum * -1;
  }
  //ASSERT:we have the correct returnNum
  return returnNum;
}





//PRE:toParse is a string that we will split up by the delimiter that
//is defined in the constants file, numArgs is the number of arguments
//that we find in the string toParse
//POST:returns an array of strings and numArgs by reference
void parser(string toParse,string parseArray[], uint & numArgs){
  string toInsert = "";
  numArgs = 0;
  uint counter = 0;
  bool done = false;
  while(!done){
    //ASSERT: we have not reached the end of string
    while(toParse[counter] != EOS && toParse[counter] != DELIMITER){
      //ASSERT:we are at a letter
      toInsert = toInsert + toParse[counter];
      counter ++;
    }
    //ASSERT:we have left the while loop, we must be at EOS or space,
    //so add an argument

    parseArray[numArgs] = toInsert;

    toInsert = "";
    numArgs ++;
    if(toParse[counter] != EOS){
      //ASSERT:we are at a space so go one more, if we are at EOS we
      //dont want to add one to counter cause well go out of scope
      counter ++;
    }
    else{
      cout << "In done" << endl; 
      done = true;
    }
  }

}

//PRE: takes a given time, and the about of seconds to be waited
//POST: will find the differance between given_time and the currently time
//      and will the differance is greater than the wait time it will return
//      true if not return false
//      need:
//        #include <time.h>
//      example:
//      time_t now;
//      time(&now);  /* get current time; same as: now = time(NULL)  */
//      while(timer(now, 5)) {
//        cout << "in while" << endl;
//      }
bool timer(time_t & given_time, double wait_time) {
 double seconds;
 time_t now;
 time(&now);  /* get current time; same as: now = time(NULL)  */
 seconds = difftime(now, given_time);
 bool return_value;
 if (seconds < wait_time) {
   return_value = true;
 } else {
   return_value = false;
 }
 return return_value;
}


//PRE:takes two sockaddr_in objects, PORT is the port you want to make
//it on 
//POST:sets the sin_family and sin_port of the addresses 
void setSockaddr(sockaddr_in & sendAddress, sockaddr_in & recieveAddress, uint port){
  sendAddress.sin_family = AF_INET;
  sendAddress.sin_port = htons(port);
  recieveAddress.sin_family = AF_INET;
  recieveAddress.sin_port = htons(port);
}


//PRE:myID is the id of your node, ip is a string representing an ip
//address, sockfd is the socket to send a message on
//POST:sends a message to the ipaddress specificed on the socket
//specified by sockfd and waits a certain amount of time, if it
//recieves a message back it returns true and exits, otherwise returns
//false
bool ping(uint myID, string ip, int & sockfd){
  bool returnVal = false;
  int valid = 0;
  sockaddr_in sendAddress;
  sockaddr_in recieveAddress;
  setSockaddr(sendAddress,recieveAddress,FORKPORT);

  valid = inet_pton(AF_INET, ip.c_str(),&sendAddress.sin_addr);
  string stringMyID = to_string(myID);
  string sendMessage = stringMyID + PING;
  //ASSERT:sendMessage now holds first yourID and then ping
  
  
  valid = sendto(sockfd, sendMessage.c_str(), sendMessage.size(), MSG_NOSIGNAL, (struct sockaddr *)&sendAddress, sizeof(sendAddress));
  //just sent message so start timer

  bool done = false;
  bool recieved = false;
  char buf[MAXRECV + 1];
  
  //set up timer stuff
  time_t now;
  time(&now);
  double wait_time = WAITTIME;
  
  while((!done) && (!recieved)){
    socklen_t sizeOfAddr;
    valid = recvfrom(sockfd,buf,MAXRECV,0,(struct sockaddr *) &recieveAddress, &sizeOfAddr);
    if(valid != -1){
      uint numArgs = 0;
      //ASSERT:we got a message, check its a pingback message, if not then need to handle it
      string message = buf;
      string messageArray[MAXWORDS];
      parser(message, messageArray, numArgs);
      if(messageArray[0] == PINGBACK){
	//ASSERT:this is correct message
	recieved = true;
	returnVal = true;
      }
      else{
	//ASSERT:is a different message, must handle


	
      }
    }
    done = timer(now,wait_time);

  }  

  return returnVal;
}



//PRE:myID is your nodeID, message is the string you recieved ip is
//the ipAddress that you recieved the
//message from, sockfd is your socket, recieveAddress and sendAddress
//are variables you need to send and recieve, myKBucket is your
//kBucket that will need to alter
//POST:depending on what is held in the message the function calls a
//different function
void handleMessage(uint myID, string message, string ip, string myIP,int & sockfd, sockaddr_in sendAddress, sockaddr_in recieveAddress, kBucket & myKBucket, bool & exit, LinkedList <int> * myKeys){
  uint numArgs = 0;
  string messageArray[MAXWORDS];
  parser(message,messageArray, numArgs);
  string messageID = messageArray[0];
  char array[BITS];
  strcpy(array,messageID.c_str());
  //ASSERT:messageArray[0] always holds the id of the person who sent
  //the msg
  uint rcvID = charToInt(array);
  //ASSERT:aTriple should be inserted if there was room in kBucket
  //level

  //messageArray[1] will always hold the cmd
  if(messageArray[COMMAND] == PING){
    cout << "Executing Ping" << endl;
    int valid = inet_pton(AF_INET,ip.c_str(), &sendAddress.sin_addr);
    string message = "pingback";
    sendto(sockfd,message.c_str(),message.size(),MSG_NOSIGNAL,(struct sockaddr * )&sendAddress,sizeof(sendAddress));
    //ASSERT:send pingback message to the person that sent ping
  }
  else if(messageArray[COMMAND] == STORE){
    cout << "Executing Store" << endl;
    //ASSERT:we recieved a store RPC
    if(ip == myIP){
      //ASSSERT:we recieved this from ourself
      string aKey = messageArray[ARGKEY];
      char keyArray[BITS];
      strcpy(keyArray,aKey.c_str());
      uint newKey = charToInt(keyArray);
      myKeys->addToBack(newKey);
      //ASSERT:we have stored the key in our linkedList
      string sendMessage = to_string(myID) + " " + myIP +  " " + STORE + " " + aKey;
      triple closest;
      initiateFindNode(sockfd, myID, newKey, myKBucket,myIP, closest);
      //cerr << "Enter send message" << endl;
      closest.sendMessage(sendMessage, sockfd);
      //cerr << "Exit send message" << endl;
    
    }
    else{
      //ASSERT:we recieved this from someone else
      string aKey = messageArray[ARGKEY + 1];
      char keyArray[BITS];
      strcpy(keyArray,aKey.c_str());
      uint newKey = charToInt(keyArray);
      myKeys->addToBack(newKey);
      //ASSERT:added the key to linkedList of keys
    }
    
  }
  else if(messageArray[COMMAND] == FINDKEY){
    //cout << "Executing Find Key" << endl;
    uint newKey = stringToInt(messageArray[ARGKEY]);
    if(ip == myIP){
      triple closest;
      initiateFindNode(sockfd, myID, newKey, myKBucket,myIP, closest);
      cout << "Stored in: " << closest.getIP() << endl;
      //call initiate findNode
    }
    else{
      handleFindNode(sockfd, ip, myID, newKey,
		     myKBucket,myIP);
    }
  }
  else if(messageArray[COMMAND] == EXIT){
      exit = true;
      cout << "Executing Exit" << endl;
  }
  else if(messageArray[COMMAND] == SHOWKBUCKETS){
    cout << "Executing showkbuckets" << endl;
    myKBucket.showKBuckets();
  }
  else if(messageArray[COMMAND] == SHUTDOWN){
    cout << "Executing shutdown" << endl;
    exit = true;
    myKBucket.shutDown(sockfd);
    //now contact yourself and shut main down before exiting
    sockaddr_in mainSendAddress;
    string message = "shutdown";
    mainSendAddress.sin_family = AF_INET;
    mainSendAddress.sin_port = htons(MAINPORT);
    int anotherValid = inet_pton(AF_INET, myIP.c_str(), &mainSendAddress.sin_addr);
    anotherValid = sendto(sockfd,message.c_str(),message.size(),MSG_NOSIGNAL,(struct sockaddr *)&mainSendAddress,sizeof(sendAddress));
    //ASSERT:sent shutdown to main
  }
  else{
    cout << "Matches no commands" << endl;
    //shouldnt be an else cause you should only receieve valid msg
    //from people soo idk why I put this but im more comfortable with
    //it this way
  }

  
}

// PRE:  Takes a socket, this nodes id, the node id that is trying to be found
//       and this nodes k buckets
// POST: Returns the node closest to targetID
void initiateFindNode(int & sockfd, uint myID, uint targetID, kBucket & mykBucket, string myIP,
		      triple & ans) {
  triple kClosest [K];
  uint traversed [K];
  for (int i = 0; i < K; i++) {
    traversed[i] = 0;
  }
  uint numK = 0;
  mykBucket.findKClosest(targetID, numK, kClosest);
  //cout << "Found k closest: " << numK << endl;
  uint kContacted = 0;
  uint kResponded = 0;
  uint aPending = 0;
  string message = to_string(myID) + " " + myIP + " " + FINDKEY + " " + to_string(targetID);
  string response;
  for (int i = 0; i < numK; i++) {
    if (i < ALPHA) {
      kClosest[i].sendMessage(message, sockfd);
      kContacted ++;
      traversed[i] ++;
      aPending ++;
    }
  }
  //cout << "Sent Messages" << endl;
  while ((kResponded != kContacted) || (kContacted < numK)) {
    try {
      string response;
      sockaddr_in recieveAddress;
      memset(&recieveAddress, 0, sizeof(recieveAddress));
      int valid = recieveMessage(response, sockfd, recieveAddress);
      if (valid == -1) {
	throw socketException("");
      }
      //cout << response << endl;
      //char ipBuf[IPLEN];
      //inet_ntop(AF_INET, &recieveAddress.sin_addr, ipBuf,IPLEN);
      //for (int i = 0; i < numK; i++) {
	//cout << "Traversed: Index " << i << " = " << traversed[i] << endl;
      //}
      uint numArgs = 0;
      uint argRead = 0;
      string command[MAXWORDS];
      //cout << "Entering Parse" << endl;
      parser(response,command,numArgs);
      //cout << "Exiting Parse" << endl;
      uint responseID = stringToInt(command[argRead]);
      argRead ++;
      string ipAddress = command[argRead];
      argRead ++;
      //cout << "Entering Refresh" << endl;
      refreshNode(responseID, ipAddress, mykBucket,sockfd);
      //cout << "Exiting Refresh" << endl;
      //cout << "Entering Compare Closest" << endl;
      compareClosest(kClosest, numK,  numArgs,
		     argRead, command, targetID, traversed, kContacted, kResponded,
		     myIP);
      //cout << "Exiting Compare Closest" << endl;
      aPending --;
      //cout << "Entering Closest Var" << endl;
      updateClosestVar(kClosest, numK, kResponded, traversed, responseID);
      //cout << "Exiting Closest Var" << endl;
    }
    catch(socketException e) {}
    if(aPending < ALPHA) {
      contactNewNode(kClosest, numK, kContacted, traversed, aPending,
		     message, sockfd);
    }
  }
  //cout << "Returning Ans" << endl;
  ans = getAns(kClosest, numK, targetID);
  //cout << "Exiting Ans" << endl;
  //cout << "Ans IP: " << ans.getIP() << endl;
  //cout << "Ans ID: " <<ans.getNodeID() << endl;
  //cout << "Ans Port: " << ans.getPort() << endl;
}


//PRE:takes a string
//POST:returns an int representing the string input
uint stringToInt(string aString){
  uint returnVal;
  char aInt[BITS];
  strcpy(aInt,aString.c_str());
  returnVal = charToInt(aInt);
  return returnVal;
}

// PRE: Takes the socket identifier the ip adress that requested the
//      find node, the ID of this node, the ID of the targetNode and
//      the kBuckets of this node
//
// POST: A message has been sent to the senderIP containing the
//       kClosest nodes to this node
void handleFindNode(int & sockfd, string senderIP,
		    uint myID, uint targetID, kBucket & mykBucket,string myIP) {
  triple kClosest [K];
  uint numK = 0;
  string currIP;
  uint currID;
  string message = to_string(myID) + " " + myIP;
  mykBucket.findKClosest(targetID, numK, kClosest);
  for (int i = 0; i < numK; i++) {
    currIP = kClosest[i].getIP();
    currID = kClosest[i].getNodeID();
    message += " " + currIP + " " + to_string(currID); 
  }
  cout << "Response Message: " << message << ": To " << senderIP << endl;
  sockaddr_in sendAddress;
  sendAddress.sin_family = AF_INET;
  sendAddress.sin_port = htons(FORKPORT);
  inet_pton(AF_INET, senderIP.c_str(), &sendAddress.sin_addr);
  sendto(sockfd, message.c_str(), message.size(), MSG_NOSIGNAL,
		 (struct sockaddr *)&sendAddress, sizeof(sendAddress));
}

// PRE:  Takes a string to store response, the socket identifier
//       and an sockaddr_in to store the receiveAddress
//
// POST: Response contains the message sent from address
//       receiveAdress and recieveAdress has the adress info
int recieveMessage(string & response, int & sockfd, sockaddr_in & receiveAddress) {
  int valid = 0;
  receiveAddress.sin_family = AF_INET;
  receiveAddress.sin_port = htons(FORKPORT);
  socklen_t sizeOfAddr = sizeof(receiveAddress);
  char buf [MAXRECV + 1];
  valid = recvfrom(sockfd,buf,MAXRECV,0,(struct sockaddr *) &receiveAddress, &sizeOfAddr);
  response = buf;
  return valid;
}

// PRE:  Takes a nodeID, a sockaddr_in containing the adress
//       for nodeID, and the kBucket for the current node
//
// POST: insertTriple is called on myKBucket for a node created
//       using nodeID and receiveAddress
void refreshNode(int nodeID, string ipAddress, kBucket & myKBucket, int & sockfd) {
  triple aTriple(ipAddress, nodeID, FORKPORT);
  myKBucket.insertTriple(aTriple,sockfd);
}

// PRE:  Takes the current kClosest and numK is the number of nodes in
//       kClosest currently, takes the number of arguments in command and
//       the location of the read pointer along with an array of all
//       the commands containing alternating ipadress and nodeid values
//       for the new kClosest, takes the nodeID for the target, the array
//       of ints denoting what has been traversed, the number of k
//       contacted and responded
//
// POST: kClosest is updated to contain the closest out of kClosest and the new
//       nodes provided by command, traversed kContacted and kResponsed are
//       updated accordingly if any nodes in kClosest that had been contacted
//       were removed, if kClosest had empty slots numK is increased to the
//       number of nodes now contained within kClosest
void compareClosest(triple kClosest [], uint & numK, uint numArgs, uint & argsRead,
		    string command [], uint targetID, uint traversed [],
		    uint & kContacted, uint & kResponded, string myIP) {
  uint count = 0;
  uint numChecked = 0;
  uint swapIndex = 0;
  uint numClosest = (numArgs - argsRead) / 2;
  triple responseClosest [numClosest];
  uint greatestDelta = -1;
  bool found = false;
  string currIP;
  int convertedID;
  int index = numClosest;
  for (int i = 0; i < index; i++) {
    currIP = command[argsRead];
    argsRead ++;
    convertedID = stringToInt(command[argsRead]);
    argsRead ++;
    /*
      --------------------------
      CHECK THIS IN TESTING
      --------------------------
    */
    
    if(checkInClosest(currIP, kClosest, numK) && currIP != myIP) {
      triple newTriple( currIP, convertedID, FORKPORT);
      responseClosest[i] = newTriple;
    }
    else{
      numClosest --;
    }
  }
  while (numK < K && count < numClosest) {
    kClosest[numK] = responseClosest[count];
    count ++;
    numK ++;
  }
  while (count < numClosest) {
    uint responseID = responseClosest[count].getNodeID();
    uint responseDelta = responseID ^ targetID;
    while(numChecked < numK) {
      uint checkID = kClosest[numChecked].getNodeID();
      uint checkDelta = checkID ^ targetID;
      if (responseDelta < checkDelta) {
	if (greatestDelta == -1) {
	  found = true;
	  greatestDelta = checkDelta;
	  swapIndex = numChecked;
	}
	else if(checkDelta > greatestDelta) {
	  greatestDelta = checkDelta;
	  swapIndex = numChecked;
	}
      }
      numChecked ++;
    }
    if (found) {
      kClosest[swapIndex] = responseClosest[count];
      if(traversed[swapIndex] == 2) {
	kContacted --;
	kResponded --;
      }
      else if (traversed[swapIndex] == 1) {
	kContacted --;
      }
      traversed[swapIndex] = 0;
    }
    numChecked = 0;
    greatestDelta = -1;
    count ++;
  }
}

// PRE:  Takes the current kClosest nodes and number of nodes in
//       kClosest, Takes the number of nodes contacted, and the
//       array denoting which nodes have been traversed along with
//       the amount of currently pending requests, finally this function
//       takes the message to send and the socket indentifier
//
// POST: The first untraversed node in kClosest has been contacted with
//       message and aPending, kContacted, and traversed are updated accordingly
//       If there were no untraversed nodes nothing happens
void contactNewNode(triple kClosest [], int numK, uint & kContacted,
	       uint traversed [], uint & aPending, string message,
	       int & sockfd) {
  bool found = false;
  int count = 0;
  while(!found && count < numK) {
    if(traversed[count] == 0) {
      found = true;
    }
    else {
      count ++;
    }
  }
  if(found) {
    //cout << "in found" << endl;
    kClosest[count].sendMessage(message, sockfd);
    traversed[count] ++;
    kContacted ++;
    aPending ++;
  }
}

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest, takes the number of nodes that have responded it
//       along with the array deonting which nodes have been
//       traversed, finally this takes the id of the node that
//       most responded to this node
//
// POST: traversed and kResponded are updated accordingly if
//       responseID is still contained within kClosest otherwise
//       nothing happens
void updateClosestVar(triple kClosest [], uint numK,
		      uint & kResponded, uint traversed [],
		      uint responseID) {
  uint loc = findInClosest(kClosest, numK, responseID);
  if (loc != -1) {
    traversed[loc] ++;
    kResponded ++;
  }
}

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest, also takes the nodeID to search for
//
// POST: if responseID is contained in kClosest the location of said
//       responseID in kClosest is returned, otherwise -1 is returned
uint findInClosest (triple kClosest [], uint numK, uint responseID) {
  bool found = false;
  uint currID;
  uint count = 0;
  while (!found && count < numK) {
    currID = kClosest[count].getNodeID();
    if (currID == responseID) {
      found = true;
    }
    else {
      count ++;
    }
  }
  if (!found) {
    count = -1;
  }
  return(count);
}

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest also takes the targetID
//
// POST: if numK > 0 returns a pointer to the triple in kClosest that
//       is closest to targetID, otherwise returns NULL
triple getAns(triple kClosest [], uint numK, uint targetID) {
  triple ans;
  //cout << numK << endl;
  if (numK > 0) {
    uint currID = kClosest[0].getNodeID();
    uint smallestDelta = currID ^ targetID;
    uint targetLoc = 0;
    uint currDelta = 0;
    for (int i = 1; i < numK; i ++) {
      currID = kClosest[i].getNodeID();
      currDelta = currID ^ targetID;
      if (currDelta < smallestDelta) {
	smallestDelta = currDelta;
	targetLoc = i;
      }
    }
    triple aTriple(kClosest[targetLoc].getIP(), kClosest[targetLoc].getNodeID(),
		   kClosest[targetLoc].getPort());
    ans = aTriple;
  }
  /*
    ----------------------------------
    CHECK IF THIS SHOULD BE &ans
    ----------------------------------
  */
  return (ans);
}

// PRE:  Takes an IP, a list of triple, and the number of triples
// POST: returns true if currIP is not in kClosest
bool checkInClosest(string currIP, triple kClosest [], uint numK) {
  bool ans = true;
  for (int i = 0; i < numK; i++) {
    if (kClosest[i].getIP() == currIP) {
      ans = false;
    }
  }
  return ans;
}
