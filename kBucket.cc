#include "kBucket.h"
#include "Constants.h"
#include "LinkedList.h"
#include <string>
#include "triple.h"
#include <iostream>
#include <math.h>       // for log2 function used in findLevel
#include "helper.h"
using namespace std;



//CLASS CI:buckets is an array of LinkedLists of type Triple which have
//a max size of K(defined in the constants file), at any
//point in time there are BITS(Defined in the constants file)
//LinkedLists  in the array that may be empty or full, all of these
//linked lists are again size K.

//constructor
kBucket::kBucket(){
  counter = 0;
  for (int i = 0; i < BITS; i++) {
    buckets[i] = new LinkedList <triple>();
  }
}

//PRE:none
//POST:creates the Kbucket object with all of the linklist in the
//array empty, makes UNodeID = pUNodeID
kBucket::kBucket(uint pUNodeID){
  uNodeID = pUNodeID;
  counter = 0;
  for (int i = 0; i < BITS; i++) {
    buckets[i] = new LinkedList <triple>();
  }
}

//PRE:takes a uint representing a nodeID
//POST:returns the level of the kBucket that the nodeID should be
//attempted to be inserted into
uint kBucket::findLevel(uint aNodeID){
  uint XOR = uNodeID ^ aNodeID;
  //ASSERT:we have the xor of the 2 nodeIDs
  uint return_value = log2(XOR);
  return return_value;
}

//PRE:takes a level of the kBucket
//POST:returns a boolean, false if the kBucket is not full and can
//be inserted into, true if the kBucket is full and cannot be
//inserted into
bool kBucket::isFull(uint level){
  LinkedList <triple> * aLinkedList = buckets[level];
  uint numTriples = aLinkedList->getNumNodes();
  bool returnVal = false;
  if(numTriples >= K){
    //ASSERT:there are max triples in the kBucket
    returnVal = true;
  }
  return returnVal;
}

//PRE:have contacted the nodeId in the triple at the head of the
//linkedlist on kBucket level and the node has not responded, thus
//we can pop this off,
//POST:pops the head of the linkedlist at bucket[level] off
void kBucket::popHead(uint level){
  LinkedList <triple> * aLinkedList = buckets[level];
  aLinkedList->deleteFirst();
  //ASSERT:this linkedList has had its head deleted
}

//PRE:have contacted the nodeID in the triple at the head of the
//linkedList of kBucket level and it has responded, thus we move it
//to the back of the linkedList
//POST:pop the head of the linkedList off, and put it at the back
void kBucket::headToTail(uint level){
  LinkedList <triple> * aLinkedList = buckets[level];
  triple aTriple = aLinkedList->getHead();
  aLinkedList->deleteFirst();
  aLinkedList->addToBack(aTriple);
  //ASSERT:the triple that was at the front has been put at the back
}

//PRE:level represents the level of the kBucket we are going to
//insert this on, aTriple represents the triple we are inserting,
//assume that there is enough space in the linkedList to insert
//this, IE the linkedlist has less than K things in it
//POST:inserts aTriple to the tail of the linkedList at bucket[level]
void kBucket::insertTriple(triple & aTriple, int & sockfd){
  uint level = this->findLevel(aTriple.getNodeID());
  //cout<<"Inserting Triple: " << aTriple.getTriple() << endl;
  LinkedList <triple> * aLinkedList = buckets[level];
  uint numNodes = aLinkedList->getNumNodes();
  if (aLinkedList->checkIndex(aTriple)) {
    uint loc = aLinkedList->findIndex(aTriple);
    aLinkedList->deleteNth(loc);
    aLinkedList->addToBack(aTriple);
  }
  else if (numNodes == K) {
    triple refreshTriple = aLinkedList->getHead();
    aLinkedList->deleteFirst();
    if(!ping(uNodeID,refreshTriple.getIP(), sockfd)) {
      aLinkedList->addToBack(aTriple);
      counter++;
    }
    else {
      aLinkedList->addToBack(refreshTriple);
    } 
  }
  else {
    aLinkedList->addToBack(aTriple);
    counter++;
  }
  //ASSERT: kBucket appropriate for aTriple is refreshed
}


//PRE:takes no parameters
//POST:prints out each level of the kBucket if there is something on
//that level, if there is nothing on a kbucket level it is ommitted,
//the level will be printed before anything that is in the kBucket
//as well
void kBucket::showKBuckets(){
  //cout<< "in showKBuckets" <<endl;
  for(uint i = 0; i < BITS; i++){
    //ASSERT:This will loop through all levels of kBuckets
    LinkedList <triple> * currBucket = buckets[i];
    uint numThings = currBucket->getNumNodes();
    if(numThings != 0){
      //ASSERT:There is stuff in the linkedList
      cout <<"Level "<<i<<":";
      for(uint j = 0; j < numThings; j++){
	triple aTriple = currBucket->getNth(j);
	string printString = aTriple.getTriple();
	cout<<printString<<","<<endl;
      }
      cout<<"----------------------------------------------------------------------------------------------"<<endl;
    }
  }  
}

//PRE: int target_id, the id trying the 
//     int & length, pointer to the length of the array of buckets
//     triple * buckets, the array of k close nodes
//POST:does not return, each parameter is passes by ref and will
//     change during the function. what is changed is explained above
void kBucket::findKClosest(uint targetID, uint & length, triple closest[]) {
  length = 0;
  uint count = 0;
  if(counter <= K) {
    //return all triples you have in all kBuckets
    while(length != counter){
      LinkedList <triple> * aLinkedList = buckets[count];
      uint len = aLinkedList->getNumNodes();
      if(len != 0){
	for(uint j = 0; j < len; j++){
	  closest[length] = aLinkedList->getNth(j);
	  length ++;
	}
      }
      //ASSERT:will exit while loop when length=count because that
      //means we got all the triples
      count ++;
    }
  } else {
    uint level = findLevel(targetID);//find the level of the target_id
    LinkedList <triple> * aLinkedList = buckets[level];
    uint len = aLinkedList->getNumNodes();
    for(uint i = 0; i < len; i++) {
      closest[length] = aLinkedList->getNth(i);
      length++;
    }
    //ASSERT:assume we need to find more triples
    bool done = false;
    if(length == K) {
      //ASSERT:we dont need to find more triples as that level was
      //full
      done = true;
    }
    
    int above = level + 1;
    int below = level - 1;
    while(!done) {
      
      //ASSERT: we have not found K triples yet and there are K
      //triples in our kBuckets
      LinkedList <triple> * aboveList;
      LinkedList <triple> * belowList;
      LinkedList <triple> * totalList;
      if(above < BITS){
	//ASSERT:we are still in scope
	aboveList = buckets[above];
      }
      if(below >= 0){
	//cout << "Before while" << endl;
	//cout << below << endl;
	//ASSERT:we are still in scope
	belowList = buckets[below];
      }
      /*
	-----------------------------------
	TEST THIS
	-----------------------------------
      */
      if (above < BITS && below >= 0) {
	totalList = new LinkedList <triple> (*aboveList + *belowList);
      }
      else if (above < BITS) {
	totalList = new LinkedList <triple> (*aboveList);
      }
      else if (below >= 0) {
	totalList = new LinkedList <triple> (*belowList);
      }
      //function to order linkedlist by distance to targetid
      //add all you can in order to closest, if length at any point
      //= K then you stop and make done true
      /*
      triple aList[BITS];
      uint totalLen = totalList->getNumNodes();
      for(uint i = 0; i < totalLen; i++){
	aList[i] = totalList->getNth(i);
	}*/
      //quickSort(aList,0,totalLen,targetID);
      //ASSERT:aList is in order now
      /*
      LinkedList <triple> newTotalList;
      for(uint i = 0; i < totalLen; i++){
	newTotalList.addToBack(aList[i]);
	}*/
      //cout << above << endl;
      //cout << below << endl;
      while((totalList->getNumNodes() != 0) && (!done)){
	if(length == K){
	  done = true;
	}
	else{
	  triple aTriple = totalList->getNth(0);
	  //cout << above << endl;
	  //cout << below << endl;
	  //cout << length << ": " << aTriple.getIP() << endl;
	  totalList->deleteNth(0);
	  closest[length] = aTriple;
	  //cout << totalList->getNumNodes() << endl;;
	  length ++;
	}
      }
      //cout << "After while" << endl;
      above ++;
      below --;
    }      
  }  
}


//PRE:takes a pointer to a array of triples and a targetID and a start
//and end value
//POST:returns by reference the list of triples in order from closest
//in distance to the targetID
void kBucket::quickSort(triple aList[], uint start, uint end, uint targetID){
  if(start < end){
    uint mid = partition(aList, start, end, targetID);
    quickSort(aList,start,mid-1,targetID);
    quickSort(aList,mid + 1, end, targetID);
  }
}

//PRE:takes a pointer to a array of triples, a start and end value and
//a targetID
//POST:returns the position the partition element was swapped into
uint kBucket::partition(triple aList[], uint start, uint end, uint targetID){
  uint left = start + 1;
  uint right = end - 1;
  bool finished = false;
  while(!finished){
    //Distance is simply the nodeID of the triple ^ targetID
    //want smaller things to be to left
    while(((aList[left].getNodeID() ^ targetID) < (aList[start].getNodeID() ^ targetID))){
      left ++;
    }
    while(((aList[right].getNodeID() ^ targetID) < (aList[start].getNodeID() ^ targetID)) && (left < right)){
      right ++;
  }
    if(right>=left){
      triple swap = aList[right];
      aList[right] = aList[left];
      aList[left] = swap;
    }
    finished = true;
  }
  
  triple swap = aList[right];
  aList[right] = aList[start];
  aList[start] = swap;
  return right;
}




//PRE:sockfd is the socket that we are sending on
//POST:calls sendMessage on every triple in this kBucket
void kBucket::shutDown(int sockfd){
  string message = "shutdown";
  for(uint i = 0; i < BITS; i ++){
    LinkedList <triple> * aLinkedList = buckets[i];
    uint len = aLinkedList->getNumNodes();
    for(uint j = 0; j < len; j ++){
      triple aTriple = aLinkedList->getNth(j);
      aTriple.sendMessage(message,sockfd);
      //ASSERT:sent shutdown message to the ip held in this triple
    }
  }
}

//destructor
kBucket::~kBucket(){
  for(int i = 0; i < BITS; i++){
    delete buckets[i];
  }
  //ASSERT:all linkedLists in bucket should be deleted
}


