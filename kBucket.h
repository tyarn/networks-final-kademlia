#ifndef INCLUDED_KBUCKET
#define INCLUDED_KBUCKET
#include <string>
#include "LinkedList.h"
#include "Constants.h"
#include "triple.h"
using namespace std;

//CLASS CI:buckets is an array of LinkedLists of type Triple which have
//a max size of K(defined in the constants file), at any
//point in time there are BITS(Defined in the constants file)
//LinkedLists  in the array that may be empty or full, all of these
//linked lists are again size K.

class kBucket{
 private:
  LinkedList <triple> *  buckets[BITS]; //buckets is an array of
				     //pointers to linkedList of type
				     //triple 
  uint uNodeID;
  uint counter;                     
 public:
  //constructor
  kBucket();

  //PRE:none
  //POST:creates the Kbucket object with all of the linklist in the
  //array empty, makes UNodeID = pUNodeID
  kBucket(uint pUNodeID);

  //PRE:takes a uint representing a nodeID
  //POST:returns the level of the kBucket that the nodeID should be
  //attempted to be inserted into
  uint findLevel(uint aNodeID);

  //PRE:takes a level of the kBucket
  //POST:returns a boolean, false if the kBucket is not full and can
  //be inserted into, true if the kBucket is full and cannot be
  //inserted into
  bool isFull(uint level);

  //PRE:have contacted the nodeId in the triple at the head of the
  //linkedlist on kBucket level and the node has not responded, thus
  //we can pop this off,
  //POST:pops the head of the linkedlist at bucket[level] off
  void popHead(uint level);

  //PRE:have contacted the nodeID in the triple at the head of the
  //linkedList of kBucket level and it has responded, thus we move it
  //to the back of the linkedList
  //POST:pop the head of the linkedList off, and put it at the back
  void headToTail(uint level);

  //PRE:level represents the level of the kBucket we are going to
  //insert this on, aTriple represents the triple we are inserting,
  //assume that there is enough space in the linkedList to insert
  //this, IE the linkedlist has less than K things in it 
  //POST:inserts aTriple to the tail of the linkedList at bucket[level]
  void insertTriple(triple & aTriple, int & sockfd);

  //PRE:takes no parameters
  //POST:prints out each level of the kBucket if there is something on
  //that level, if there is nothing on a kbucket level it is ommitted,
  //the level will be printed before anything that is in the kBucket
  //as well
  void showKBuckets();

  
  //PRE: int target_id, the id trying the 
  //     int & length, pointer to the length of the array of buckets
  //     triple * buckets, the array of k close nodes
  //POST:does not return, each parameter is passes by ref and will
  //     change during the function. what is changed is explained above
  void findKClosest(uint targetID, uint & length, triple closest[]);


  //PRE:takes a pointer to a array of triples and a targetID and a start
  //and end value
  //POST:returns by reference the list of triples in order from closest
  //in distance to the targetID
  void quickSort(triple list[], uint start, uint end, uint targetID);

  //PRE:takes a pointer to a array of triples, a start and end value and
  //a targetID
  //POST:returns the position the partition element was swapped into
  uint partition(triple aList[], uint start, uint end, uint targetID);
 

  //PRE:sockfd is the socket that we are sending on
  //POST:calls sendMessage on every triple in this kBucket
  void shutDown(int sockfd);


  //destructor
  ~kBucket();
  
};

#endif
